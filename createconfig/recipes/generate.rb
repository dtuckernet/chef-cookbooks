node[:deploy].each do |app_name, deploy_config|
  # determine root folder of new app deployment
  app_root = "#{deploy_config[:deploy_to]}/current"

  Chef::Log.info("Running Create Config with app_root of [#{app_root}]")

  directory app_root do
    group deploy_config[:group]
    owner deploy_config[:user]
    recursive true
  end

  # use template 'config.json.erb' to generate 'config-opsworks.json'
  template "#{app_root}/config-opsworks.json" do
    source "config.json.erb"
    cookbook "createconfig"

    # set mode, group and owner of generated file
    mode "0660"
    group deploy_config[:group]
    owner deploy_config[:user]

    # define variable “@settings” to be used in the ERB template
    variables(
      :settings => deploy_config[:custom_settings] || {}
    )

    # only generate a file if there is custom configuration
    not_if do
      deploy_config[:custom_settings].blank?
    end
  end
end